const databaseService = require('../../utils/databaseService.js');
const utilityService = require('../../utils/utilityService.js');

/**
 * Fetches all registered users from the database if they're not assigned to project yet and if they're not the requesting user
 * @param decoded {Object}
 * @returns {Promise}
 */
const getAllRegisteredUsers = decoded => {
	return new Promise((resolve, reject) => {
		let users = null, projects = null;
		return Promise.all([
			databaseService.getAllUsers(),
			databaseService.getAllProjects()
		])
			.then(res => {
				users = res[0].map(user => user.dataValues);
				projects = res[1].map(project => project.dataValues);
				resolve(users.filter(user =>
					!projects.find(project => utilityService.decodeJSON(project.members).find(member => member.id === user.id)) && user.id !== decoded.id
				).map(user => ( {
					id: user.id,
					email: user.email,
					fullName: user.fullName
				} )))
			})
			.catch(err => reject(err));
	})
};

module.exports = {
	getAllRegisteredUsers
};