const utilityService = require('../../utils/utilityService.js');
const databaseService = require('../../utils/databaseService.js');
const moment = require('moment');

/**
 * Fetches all projects for the professor
 * @param decoded {Object}
 * @returns {Promise}
 */
const getProjects = decoded => {
	return new Promise((resolve, reject) => {
		return databaseService.getUserByField({ field: 'id', value: decoded.id })
			.then(user => {
				if (!user.isAdmin) {
					return reject(new utilityService.createError(new Error('Only the professor can see this list'), 'Only the professor can see this list', global.HTTP_UNAUTHORIZED, 'getProjects'))
				}
				return databaseService.getAllProjects()
					.then(projects => resolve(projects.map(p => {
						const project = p.dataValues;
						return ( {
							...project,
							members: utilityService.decodeJSON(project.members),
							deliverables: utilityService.decodeJSON(project.deliverables).map(deliverable => {
								const grades = deliverable.grades.map(grade => grade.grade).filter(grade => grade);
								let averageGrade = 0;
								if (( grades || [] ).length < 3) {
									grades.forEach(grade => averageGrade += grade);
									averageGrade = averageGrade / ( grades || [] ).length
								} else {
									grades.sort((a, b) => a > b);
									grades.pop();
									grades.shift();
									grades.forEach(grade => averageGrade += grade);
									averageGrade = averageGrade / ( grades || [] ).length
								}
								return ( {
									...deliverable,
									grades: deliverable.grades.map(grade => grade.grade),
									grade: ( averageGrade || 0 ).toFixed(2),
									isCompleted: moment().isAfter(moment(deliverable.endTime)),
									canBeGraded: !( moment().isAfter(moment(deliverable.endTime).add(5, 'days')) ),
								} )
							})
						} )
					})))
			})
			.catch(err => reject(err));
	});
};

module.exports = {
	getProjects
};