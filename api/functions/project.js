const utilityService = require('../../utils/utilityService.js');
const databaseService = require('../../utils/databaseService.js');
const moment = require('moment');

/*
		let deliverables = utilityService.getDeliverables().map(deliverable => ( {
			...deliverable,
			isCompleted: moment().isAfter(moment(deliverable.endTime)),
			canBeGraded: !( moment().isAfter(moment(deliverable.endTime).add(5, 'days')) )
		} ));
		 */

/**
 * Creates a new project
 * @param newProjectData
 * @param decoded
 * @returns {Promise}
 */
const createProject = (newProjectData, decoded) => {
	return new Promise((resolve, reject) => {
		let user = null, project = null;
		let deliverables = utilityService.getDeliverables();
		return Promise.all([
			databaseService.getUserByField({ field: 'id', value: decoded.id }),
			databaseService.getProjectByField({ field: 'adminId', value: decoded.id })
		])
			.then(res => {
				user = res[0];
				project = res[1];
				if (project) {
					return reject(new utilityService.createError(new Error('You cannot create more than one project or be part of more than one project'), 'You cannot create more than one project or be part of more than one project', global.HTTP_BAD_REQUEST, 'createProject'))
				}
				const projectData = {
					adminId: user.id,
					title: newProjectData.title,
					description: newProjectData.description,
					members: utilityService.encodeJSON([ {
						id: user.id,
						email: user.email,
						fullName: user.fullName
					}, ...newProjectData.members ]),
					deliverables: utilityService.encodeJSON(deliverables),
					jury: utilityService.encodeJSON([])
				};
				return databaseService.saveProject(projectData);
			})
			.then(project => resolve({
				...project,
				members: utilityService.decodeJSON(project.members),
				deliverables: utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
					id: deliverable.id,
					description: deliverable.description,
					endTime: deliverable.endTime,
					link: deliverable.link,
					isCompleted: moment().isAfter(moment(deliverable.endTime))
				} ))
			}))
			.catch(err => reject(err))
	})
};

/**
 * Fetches the assigned project from the database
 * @param decoded {Object}
 * @returns {Promise}
 */
const getProject = decoded => {
	return new Promise((resolve, reject) => {
		return databaseService.getAllProjects()
			.then(projects => {
				const project = ( projects.find(project => project.adminId === decoded.id) || {} ).dataValues || null;
				if (project) {
					return resolve({
						id: project.id,
						title: project.title,
						description: project.description,
						members: utilityService.decodeJSON(project.members),
						deliverables: utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
							id: deliverable.id,
							description: deliverable.description,
							endTime: deliverable.endTime,
							link: deliverable.link,
							isCompleted: moment().isAfter(moment(deliverable.endTime))
						} ))
					})
				} else {
					let foundProject = null;
					projects.forEach(p => {
						const project = p.dataValues;
						let members = utilityService.decodeJSON(project.members);
						if (foundProject) return;
						if (members.find(member => member.id === decoded.id)) {
							foundProject = {
								id: project.id,
								title: project.title,
								description: project.description,
								members: utilityService.decodeJSON(project.members),
								deliverables: utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
									id: deliverable.id,
									description: deliverable.description,
									endTime: deliverable.endTime,
									link: deliverable.link,
									isCompleted: moment().isAfter(moment(deliverable.endTime))
								} ))
							}
						}
					});
					if (foundProject) {
						return resolve(foundProject)
					} else {
						return resolve();
					}
				}
			})
			.catch(err => reject(err));
	})
};

/**
 * Saved a deliverable's link for a project
 * @param newProjectData {Object}
 * @param decoded {Object}
 * @returns {Promise}
 */
const saveDeliverableLink = (newProjectData, decoded) => {
	return new Promise((resolve, reject) => {
		let user = null, project = null;
		return Promise.all([
			databaseService.getUserByField({ field: 'id', value: decoded.id }),
			databaseService.getProjectByField({ field: 'id', value: newProjectData.projectId })
		])
			.then(res => {
				user = res[0];
				project = res[1];

				let deliverables = utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
					...deliverable,
					isCompleted: moment().isAfter(moment(deliverable.endTime)),
					canBeGraded: !( moment().isAfter(moment(deliverable.endTime).add(5, 'days')) )
				} ));

				if (deliverables.find(deliverable => deliverable.id === newProjectData.deliverableId && deliverable.isCompleted === false)) {
					const newDeliverables = utilityService.encodeJSON(deliverables.map(deliverable => ( {
						...deliverable,
						link: deliverable.id === newProjectData.deliverableId ? newProjectData.link : deliverable.link
					} )));
					return databaseService.updateProjectById(newProjectData.projectId, { deliverables: newDeliverables })
				} else {
					return reject(new utilityService.createError(new Error('You cannot edit this deliverable any longer'), 'You cannot edit this deliverable any longer', global.HTTP_UNAUTHORIZED, 'saveDeliverableLink'));
				}
			})
			.then(() => resolve())
			.catch(err => reject(err));
	})
};

/**
 * Saved a deliverable's grade for a project
 * @param newProjectData {Object}
 * @param decoded {Object}
 * @returns {Promise}
 */
const saveDeliverableGrade = (newProjectData, decoded) => {
	return new Promise((resolve, reject) => {
		let user = null, project = null;
		return Promise.all([
			databaseService.getUserByField({ field: 'id', value: decoded.id }),
			databaseService.getProjectByField({ field: 'id', value: newProjectData.projectId })
		])
			.then(res => {
				user = res[0];
				project = res[1];

				let deliverables = utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
					...deliverable,
					isCompleted: moment().isAfter(moment(deliverable.endTime)),
					canBeGraded: !( moment().isAfter(moment(deliverable.endTime).add(5, 'days')) )
				} ));

				if (deliverables.find(deliverable => deliverable.id === newProjectData.deliverableId && deliverable.canBeGraded === true)) {
					const newDeliverables = utilityService.encodeJSON(deliverables.map(deliverable => {
						if (deliverable.id === newProjectData.deliverableId) {
							let newGrades;
							if (deliverable.grades.find(grade => grade.juryId === decoded.id)) {
								newGrades = deliverable.grades.map(grade => ( {
									...grade,
									grade: grade.juryId === decoded.id ? newProjectData.grade : grade.grade
								} ))
							} else {
								newGrades = [
									...deliverable.grades,
									{
										juryId: decoded.id,
										grade: newProjectData.grade
									}
								]
							}
							return ( {
								...deliverable,
								grades: newGrades
							} )
						}
						return deliverable
					}));
					return databaseService.updateProjectById(newProjectData.projectId, { deliverables: newDeliverables })
				} else {
					return reject(new utilityService.createError(new Error('You cannot edit this deliverable any longer'), 'You cannot edit this deliverable any longer', global.HTTP_UNAUTHORIZED, 'saveDeliverableLink'));
				}
			})
			.then(() => resolve())
			.catch(err => reject(err));
	})
};

module.exports = {
	createProject,
	getProject,
	saveDeliverableLink,
	saveDeliverableGrade
};