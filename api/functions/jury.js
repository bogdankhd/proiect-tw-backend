const utilityService = require('../../utils/utilityService.js');
const databaseService = require('../../utils/databaseService.js');
const moment = require('moment');

/**
 * Fetches the juried project for a user
 * @param decoded {Object}
 * @returns {Promise}
 */
const getJuriedProject = decoded => {
	return new Promise((resolve, reject) => {
		return databaseService.getAllProjects()
			.then(projects => {
				let foundProject = null;
				projects.forEach(p => {
					const project = p.dataValues;
					let jury = utilityService.decodeJSON(project.jury);
					if (foundProject) return;
					if (jury.find(id => id === decoded.id)) {
						foundProject = {
							id: project.id,
							title: project.title,
							description: project.description,
							members: utilityService.decodeJSON(project.members),
							deliverables: utilityService.decodeJSON(project.deliverables).map(deliverable => ( {
								id: deliverable.id,
								description: deliverable.description,
								endTime: deliverable.endTime,
								link: deliverable.link,
								isCompleted: moment().isAfter(moment(deliverable.endTime)),
								canBeGraded: !( moment().isAfter(moment(deliverable.endTime).add(5, 'days')) ),
								grade: (deliverable.grades.find(grade => grade.juryId === decoded.id) || {}).grade || ''
							} ))
						}
					}
				});
				if (foundProject) {
					return resolve(foundProject)
				} else {
					return resolve();
				}
			})
			.catch(err => reject(err));
	})
};

module.exports = {
	getJuriedProject
};