const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const utilityService = require('../../utils/utilityService.js');
const databaseService = require('../../utils/databaseService.js');

/**
 * Hash the password before insertion
 * @param password String
 * @returns {Promise}
 */
const hashPassword = password => {
	return new Promise((resolve, reject) => {
		bcrypt.genSalt((saltError, salt) => {
			if (saltError) {
				reject(new utilityService.createError(new Error(), saltError, global.HTTP_BAD_REQUEST, 'hashPassword'));
			}
			bcrypt.hash(password, salt, (hashError, hash) => {
				if (hashError) {
					reject(new utilityService.createError(new Error(), hashError, global.HTTP_BAD_REQUEST, 'hashPassword'));
				}
				resolve(hash);
			})
		});
	});
};

/**
 * Register a user
 * @param newUserData Object
 * @returns {Promise}
 */
const register = newUserData => {
	return new Promise((resolve, reject) => {
		let token = '', savedUser = {};

		return databaseService.getAllUsers()
			.then(users => {
				if (users.find(user => user.email === newUserData.email)) {
					return reject(new utilityService.createError(new Error('An user with this e-mail address already exists'), 'An user with this e-mail address already exists', global.HTTP_BAD_REQUEST, 'register'));
				}

				return hashPassword(newUserData.password)
			})
			.then(hash => {
				const data = {
					email: newUserData.email,
					fullName: newUserData.fullName,
					password: hash
				};

				return databaseService.saveUser(data)
			})
			.then(user => {
				savedUser = user;
				const metadata = {
					id: user.id,
					email: user.email
				};

				const expire = {};
				token = jwt.sign(metadata, global.JWT_SECRET, expire);
			})
			.then(() => {
				resolve({
					token: token,
					id: savedUser.id,
					email: savedUser.email,
					fullName: savedUser.fullName
				});
			})
			.catch(err => reject(err));
	});
};

/**
 * Perform login for a user
 * @param loginUser Object
 * @returns {Promise}
 */
const login = loginUser => {
	return new Promise((resolve, reject) => {
		return databaseService.getUserByField({ field: 'email', value: loginUser.email }).then(user => {
			bcrypt.compare(loginUser.password, user.password, (err, valid) => {
				if (err || !valid) {
					return reject(new utilityService.createError(new Error(), 'Incorrect login credentials', global.HTTP_UNAUTHORIZED, 'login'))
				}
				const metadata = {
					id: user.id,
					email: user.email
				};
				const expire = {};
				const token = jwt.sign(metadata, global.JWT_SECRET, expire);

				resolve({
					token: token,
					id: user.id,
					fullName: user.fullName,
					email: user.email,
					isAdmin: user.isAdmin
				})
			});
		}).catch(err => reject(err))
	});
};

/**
 * Fetch the credentials of a user
 * @param id {String}
 * @returns {Promise}
 */
const getCredentials = id => {
	return new Promise((resolve, reject) => {
		return databaseService.getUserByField({ field: 'id', value: id })
			.then(user => resolve({
				userId: user.id,
				email: user.email,
				fullName: user.fullName,
				isAdmin: user.isAdmin
			}))
			.catch(err => reject(err));
	})
};


module.exports = {
	register,
	login,
	getCredentials
};