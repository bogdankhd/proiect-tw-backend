const validatorService = require('../../utils/validatorService.js');
const utilityService = require('../../utils/utilityService.js');
const projectFunctions = require('../functions/project.js');

// Controller for POST /project
const createProject = (req, res) => {
	validatorService.validateInput.hasBody(req)
		.then(body => validatorService.validateSchema(body, validatorService.schemas.createProject))
		.then(project => projectFunctions.createProject(project, req.decoded))
		.then(data => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The project has been created!',
				data: data
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

// Controller for GET /project
const getProject = (req, res) => {
	return projectFunctions.getProject(req.decoded)
		.then(data => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The project has been fetched!',
				data: data
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

// Controller for PUT /project/link
const saveDeliverableLink = (req, res) => {
	return validatorService.validateInput.hasBody(req)
		.then(body => validatorService.validateSchema(body, validatorService.schemas.saveDeliverableLink))
		.then(newProjectData => projectFunctions.saveDeliverableLink(newProjectData, req.decoded))
		.then(() => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The link has been saved!'
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

// Controller for PUT /project/grade
const saveDeliverableGrade = (req, res) => {
	return validatorService.validateInput.hasBody(req)
		.then(body => validatorService.validateSchema(body, validatorService.schemas.saveDeliverableGrade))
		.then(newProjectData => projectFunctions.saveDeliverableGrade(newProjectData, req.decoded))
		.then(() => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The grade has been saved!'
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

module.exports = {
	createProject,
	getProject,
	saveDeliverableLink,
	saveDeliverableGrade
};
