const validatorService = require('../../utils/validatorService.js');
const utilityService = require('../../utils/utilityService.js');
const juryFunctions = require('../functions/jury.js');

// Controller for GET /juried-project
const getJuriedProject = (req, res) => {
	return juryFunctions.getJuriedProject(req.decoded)
		.then(data => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The juried project has been fetched!',
				data: data
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

module.exports = {
	getJuriedProject
};
