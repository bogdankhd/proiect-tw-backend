const utilityService = require('../../utils/utilityService.js');
const usersFunctions = require('../functions/users.js');

// Controller for GET /credentials
const getAllRegisteredUsers = (req, res) => {
	return usersFunctions.getAllRegisteredUsers(req.decoded)
		.then(data => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'Successfully fetched all users!',
				data: data
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

module.exports = {
	getAllRegisteredUsers
};