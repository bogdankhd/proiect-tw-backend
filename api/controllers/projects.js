const utilityService = require('../../utils/utilityService.js');
const projectsFunctions = require('../functions/projects.js');

// Controller for GET /projects
const getProjects = (req, res) => {
	return projectsFunctions.getProjects(req.decoded)
		.then(projects => {
			res.status(global.HTTP_SUCCESS).jsonp({
				status: 'success',
				message: 'The projects have been fetched!',
				data: projects
			});
			utilityService.logAction(req, req.startTime, `${ req.decoded.email } __time__`, res);
		})
		.catch(err => utilityService.logResponse(res, err));
};

module.exports = {
	getProjects
};