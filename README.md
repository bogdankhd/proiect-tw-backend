## Pre-requires

- NodeJS v12.13.0
- npm v6.13.4
- Apache
- MySQL

## Recommended

Use `xampp` if you are on a Windows device;

Download the latest version from <a href="https://www.apachefriends.org/ro/download.html">https://www.apachefriends.org/ro/download.html</a>

Install it and start Apache and MySQl.

Click on Admin next to MySQL and proceeed to the next step.

## Install

Import the `web_tech_dump.sql` into your MySQL Database using PhpMyAdmin or a tool of your own choice.

Create a database user with the following credentials:

`username: web_tech_user`

`password: qwerty123uiop`

If you wish to change the credentials, do so in the `/utils/utilityService.js` lines 7 and 8

In the root folder run:

### `npm install`

## Change deliverables dates

You can do so in `/utils/utilityService.js` in the function starting at line 110

## Startup

First <b>start MySQL</b>.

In the root folder run:

### `npm start`

## Information

All API Endpoints can be found in `/api/swagger/swagger.yaml`