const moment = require('moment');

/**
 * Maps all global variables
 */
const autoImportEnvVariables = () => {
	global.DB_USERNAME = 'web_tech_user';
	global.DB_PASSWORD = 'qwerty123uiop';
	global.DB_NAME = 'web_tech';
	global.JWT_SECRET = '673A58DE2525A2B775B72CE70220A41BEC283D922701510AA505361B1D5D8004';
	global.HTTP_SUCCESS = 200;
	global.HTTP_BAD_REQUEST = 400;
	global.HTTP_INTERNAL_SERVER_ERROR = 500;
	global.HTTP_UNAUTHORIZED = 401;
};

/**
 * Send standard errors
 * @param errObject default value 'An unexpected error has occurred'
 * @param message default value 'Unknown Error'
 * @param httpCode default 500
 * @param thrower default internal-module ( unspecified module )
 * @param extraField
 * @constructor
 */
const createError = function (
	errObject = new Error(),
	message = 'Unhandled error',
	httpCode = HTTP_INTERNAL_SERVER_ERROR,
	thrower = 'internal-module',
	extraField) {
	if (!( errObject instanceof Error )) {
		errObject = new Error(errObject);
	}
	if (Error.captureStackTrace) {
		Error.captureStackTrace(this, this.constructor);
	}
	this.stack = ( errObject ).stack;
	this.thrower = thrower;
	this.message = message;
	this.httpCode = httpCode;
	if (extraField) {
		this.extra = extraField;
	}
};

/**
 * Logs a request
 * @param req
 * @param start
 * @param str
 * @param res
 * @returns {*}
 */
const logAction = (req, start, str, res) => {
	const now = new Date();
	const time = now.getTime() - start;
	str = str.replace('__time__', time + 'ms');
	global.actionLogger.info({
		message: str || 'no-data',
		route: req.originalUrl,
		email: req.decoded && req.decoded.email || 'no-email',
		clientIp: req.clientIp,
		method: req.method,
		status: res.statusCode
	});
};

/**
 * Log error and respond
 * @param res response parameter from request
 * @param err error that has occurred
 */
const logResponse = (res, err) => {
	if (!( err instanceof Object ) || !err.httpCode) {
		err = new createError(err);
	}
	const mes = getMessage(err);
	global.errorLogger.error({
		message: err.message,
		stack: err.stack
	});
	if (res) {
		const httpCode = err.httpCode > 100 && err.httpCode < 999 ? parseInt(err.httpCode) : 'no-code';
		return res.status(( Number.isInteger(httpCode) ? httpCode : HTTP_INTERNAL_SERVER_ERROR )).jsonp({ error: mes.message });
	}
};

/**
 * Set error message
 * @param errObject
 */
const getMessage = errObject => {
	let message = {};
	message['data'] = errObject.stack;
	message['message'] = errObject.message;
	message['code'] = errObject.httpCode;
	message['thrower'] = errObject.thrower;
	if (errObject.extra) {
		message['extra'] = errObject.extra;
	}

	return message;
};

/**
 * Get the standard deliverables for all projects
 * @returns {[{link: string, description: string, id: string, endTime: (*|moment.Moment)}, {link: string, description: string, id: string, endTime: (*|moment.Moment)}, {link: string, description: string, id: string, endTime: (*|moment.Moment)}, {link: string, description: string, id: string, endTime: (*|moment.Moment)}]}
 */
const getDeliverables = () => {
	return [ {
		id: 'deliverable-1',
		description: 'Git repository has been sent',
		endTime: moment('05.01.2020', 'DD.MM.YYYY'),
		link: '',
		grades: []
	}, {
		id: 'deliverable-2',
		description: 'Minimal frontend and backend',
		endTime: moment('06.01.2020', 'DD.MM.YYYY'),
		link: '',
		grades: []
	}, {
		id: 'deliverable-3',
		description: 'Full application',
		endTime: moment('07.01.2020', 'DD.MM.YYYY'),
		link: '',
		grades: []
	}, {
		id: 'deliverable-4',
		description: 'Working application with bugs fixed',
		endTime: moment('08.01.2020', 'DD.MM.YYYY'),
		link: '',
		grades: []
	} ]
};

/**
 * Encodes an array or object into a string
 * @param variable {Array|Object}
 * @returns {string}
 */
const encodeJSON = variable => JSON.stringify(variable);

/**
 * Decodes a string into an array or object
 * @param variable {String}
 * @returns variable {Array|Object}
 */
const decodeJSON = variable => JSON.parse(variable);

/**
 * Source: https://stackoverflow.com/questions/7158654/how-to-get-random-elements-from-an-array
 * Gets random items from an array
 * @param arr {Array}
 * @param count {Number}
 * @returns {*}
 */
const getRandomArrayElements = (arr, count) => {
	if (count > (arr || []).length) {
		return arr;
	}
	let shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
	while (i-- > min) {
		index = Math.floor(( i + 1 ) * Math.random());
		temp = shuffled[index];
		shuffled[index] = shuffled[i];
		shuffled[i] = temp;
	}
	return shuffled.slice(min);
};

module.exports = {
	autoImportEnvVariables,
	createError,
	logAction,
	logResponse,
	getMessage,
	getDeliverables,
	encodeJSON,
	decodeJSON,
	getRandomArrayElements
};
