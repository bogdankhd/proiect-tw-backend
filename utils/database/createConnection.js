const sequelize = require('sequelize');
const mysql = require('mysql2/promise.js');
const utilityService = require('../utilityService.js');
const moment = require('moment');
const CronJob = require('cron').CronJob;

/**
 * Function to connect to the database
 */
const connectToDatabase = () => {
	mysql.createConnection({
		user: global.DB_USERNAME,
		password: global.DB_PASSWORD
	}).then(connection => {
		global.DB_CONNECTION = connection;
		return global.DB_CONNECTION.query(`CREATE DATABASE IF NOT EXISTS ${ global.DB_NAME }`)
	}).then(() => {
		global.SEQUELIZE = new sequelize(global.DB_NAME, global.DB_USERNAME, global.DB_PASSWORD, {
			dialect: 'mysql',
			logging: false
		});
		assignJury();
		console.log('Connected to MySQL');
		global.DB_CONNECTION.end();
	})
		.catch(err => console.error('Could not start up the MySQL connection', err.stack))
};

/**
 * A cronjob that runs every minute to check if a jury should be assigned to projects
 */
const assignJury = () => {
	const databaseService = require('../databaseService.js');
	new CronJob('* * * * *', () => {
	let users = [], projects = [];
	const deliverables = utilityService.getDeliverables();
	if (moment().isAfter(moment(deliverables[0].endTime))) {
		return Promise.all([
			databaseService.getAllProjects(),
			databaseService.getAllUsers()
		])
			.then(res => {
				projects = res[0].map(project => ( {
					...project.dataValues,
					members: utilityService.decodeJSON(project.dataValues.members),
					deliverables: utilityService.decodeJSON(project.dataValues.deliverables),
					jury: utilityService.decodeJSON(project.dataValues.jury)
				} ));
				users = res[1].map(user => user.dataValues);

				// Users that are not assigned yet in a jury
				let availableJuryMembers = users.filter(user => !projects.find(project => project.jury.find(juryId => juryId === user.id))).map(user => user.id);

				let promises = [];
				projects.forEach(project => {
					// If no jury is assigned yet, else ignore those that have a jury assigned
					if (( project.jury || [] ).length === 0) {
						let canJuryThisProject = availableJuryMembers.filter(availableJuryMember => !project.members.find(member => member.id === availableJuryMember));
						let jury = utilityService.getRandomArrayElements(canJuryThisProject, 5);

						// Now they are assigned so remove them from possible jury members
						availableJuryMembers = availableJuryMembers.filter(availableJuryMember => !jury.find(jury => jury === availableJuryMember));

						promises.push(databaseService.updateProjectById(project.id, { jury: utilityService.encodeJSON(jury) }));
					}
				});
				return Promise.all(promises);
			})
			.catch(err => console.error(err));
	}
	}, null, true, 'America/Los_Angeles');
};

module.exports = {
	connectToDatabase
};
