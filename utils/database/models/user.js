const sequelize = require('sequelize');

const UserModel = global.SEQUELIZE.define('user', {
	email: sequelize.STRING,
	password: sequelize.STRING,
	fullName: sequelize.STRING,
	isAdmin: sequelize.BOOLEAN
}, {
	timestamps: false,
	tableName: 'users'
});

module.exports = UserModel;