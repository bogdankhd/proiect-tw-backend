const sequelize = require('sequelize');

const ProjectModel = global.SEQUELIZE.define('project', {
	adminId: sequelize.INTEGER,
	title: sequelize.STRING,
	description: sequelize.STRING(1001),
	members: sequelize.STRING(10000),
	jury: sequelize.STRING(10000),
	deliverables: sequelize.STRING(10000)
}, {
	timestamps: false,
	tableName: 'projects'
});

module.exports = ProjectModel;