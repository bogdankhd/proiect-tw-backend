const validatorService = require('./validatorService.js');
const utilityService = require('./utilityService.js');
const UserModel = require('./database/models/user.js');
const ProjectModel = require('./database/models/project.js');

/**
 * Function to save a user in the database
 * @param data
 * @returns {Promise}
 */
const saveUser = data => {
	return new Promise((resolve, reject) => {
		validatorService.validateObject(data, 'data is required').catch(err => reject(err));
		return global.SEQUELIZE.sync()
			.then(() => {
				let newUser = new UserModel(data);

				return newUser.save()
					.then(savedUser => validatorService.validateObject(savedUser.dataValues, 'The user does not exist').then(user => resolve(user)).catch(err => reject(err)))
					.catch(err => reject(new utilityService.createError(new Error(), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	});
};

/**
 * Fetches all users from the database
 * @returns {Promise}
 */
const getAllUsers = () => {
	return new Promise((resolve, reject) => {
		return global.SEQUELIZE.sync()
			.then(() => {
				return UserModel.findAll()
					.then(users => validatorService.validateArray(users, 'Could not get all users').then(users => resolve(users)).catch(err => reject(err)))
					.catch(err => reject(new utilityService.createError(new Error(), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	})
};

/**
 * Fetches a user by its field
 * @param data
 * @returns {Promise}
 */
const getUserByField = data => {
	return new Promise((resolve, reject) => {
		return global.SEQUELIZE.sync()
			.then(() => {
				return UserModel.findOne({ where: { [data.field]: data.value } })
					.then(user => validatorService.validateObject(user, 'The user does not exist').then(user => resolve(user)).catch(err => reject(err)))
					.catch(err => reject(new utilityService.createError(new Error('The user does not exist'), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	})
};

/**
 * Saved a new project in the database
 * @param data {Object}
 * @returns {Promise}
 */
const saveProject = data => {
	return new Promise((resolve, reject) => {
		validatorService.validateObject(data, 'data is required').catch(err => reject(err));
		return global.SEQUELIZE.sync()
			.then(() => {
				let newProject = new ProjectModel(data);
				return newProject.save()
					.then(savedProject => validatorService.validateObject(savedProject.dataValues, 'The project does not exist').then(user => resolve(user)).catch(err => reject(err)))
					.catch(err => reject(new utilityService.createError(new Error(), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	});
};

/**
 * Fetches a project by a field
 * @param data {Object}
 * @returns {Promise}
 */
const getProjectByField = data => {
	return new Promise((resolve, reject) => {
		return global.SEQUELIZE.sync()
			.then(() => {
				return ProjectModel.findOne({ where: { [data.field]: data.value } })
					.then(project => resolve(project))
					.catch(err => reject(new utilityService.createError(new Error('The project does not exist'), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	})
};

/**
 * Fetches all projects from the database
 * @returns {Promise}
 */
const getAllProjects = () => {
	return new Promise((resolve, reject) => {
		return global.SEQUELIZE.sync()
			.then(() => {
				return ProjectModel.findAll()
					.then(projects => resolve(projects))
					.catch(err => reject(new utilityService.createError(new Error('An error has occurred while fetching all projects'), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	})
};

/**
 * Updates a project by its id
 * @param id {Number}
 * @param data {Object}
 * @returns {Promise}
 */
const updateProjectById = (id, data) => {
	return new Promise((resolve, reject) => {
		validatorService.validateObject(data, 'data is required').catch(err => reject(err));
		return global.SEQUELIZE.sync()
			.then(() => {
				return ProjectModel.update(data, {
					where: { id: id }
				})
					.then(() => resolve())
					.catch(err => reject(new utilityService.createError(new Error(), err, global.HTTP_BAD_REQUEST, 'database-service')))
			})
	})
};

module.exports = {
	saveUser,
	getAllUsers,
	getUserByField,
	saveProject,
	getProjectByField,
	getAllProjects,
	updateProjectById
};